﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketMCClass
{
    public class ClientSocketInfo
    {
        //public string server_address { get; set; }
        //public string client_address { get; set; }
        //public string userID { get; set; }
        public string groupID { get; set; }
    }
    public static class StaticClass
    {
        public const int MAX_RECEIVE_LENGTH = 2; // 2 이상으로 설정

        public const byte SOH = 1;
        public const byte SOT = 2;
        public const byte EOTR = 4;

        // 주소로 IPEndPoint 구하기
        public static IPEndPoint GetIPEndPoint(String address, String port)
        {
            IPEndPoint ipEndPoint = null;
            IPHostEntry ipHostInfo = Dns.GetHostEntry(address);
            foreach (IPAddress i in ipHostInfo.AddressList)
            {
                if (i.AddressFamily == AddressFamily.InterNetwork) ipEndPoint = new IPEndPoint(i, int.Parse(port));
            }
            return ipEndPoint;
        }
        
    }
    public class DataArg<T> : EventArgs
    {
        private T data;
        public DataArg(T data)
        {
            Data = data;
        }
        public T Data { get => data; set => data = value; }
    }
    public class LogMessage : EventArgs
    {
        private string message;
        public LogMessage(string message)
        {
            Message = message;
        }

        public string Message { get => message; set => message = value; }
    }
}
