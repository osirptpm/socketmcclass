﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SocketMCClass.ClientSocket
{
    public class CSocketMC
    {
        Socket socket;
        IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 55551);
        bool IsConnected;

        IPEndPoint serverIP;
        string groupID;
        
        byte[] receiveBuffer = new byte[StaticClass.MAX_RECEIVE_LENGTH];
        List<ArraySegment<byte>> receiveBuffers = new List<ArraySegment<byte>>();
        List<byte> receiveBytes = new List<byte>();
        
        public string GroupID { get => groupID; set => groupID = value; }

        //받은 바이트 처리
        private byte[] ReceiveBuffer { get => receiveBuffer; set => receiveBuffer = value; }
        private List<ArraySegment<byte>> ReceiveBuffers { get => receiveBuffers; set => receiveBuffers = value; }
        private List<byte> ReceiveBytes { get => receiveBytes; set => receiveBytes = value; }
        
        public event EventHandler OnConnect;
        public event EventHandler OnReceive;
        public event EventHandler OnSend;
        public event EventHandler OnDisconnect;
        public event EventHandler OnLog;

        public CSocketMC()
        {
            ReceiveBuffers.Add(new ArraySegment<byte>(ReceiveBuffer, 0, StaticClass.MAX_RECEIVE_LENGTH));
            try
            {
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            }
            catch (Exception ex)
            {
                OnLog?.Invoke(this, new LogMessage("Client Start error : " + ex.Message));
            }
        }

        public CSocketMC(IPEndPoint ipEndPoint) : this()
        {
            this.ipEndPoint = ipEndPoint;
        }
        public CSocketMC(string ipAddress, string portNumber) : this()
        {
            ipEndPoint = StaticClass.GetIPEndPoint(ipAddress, portNumber);
        }

        public void Connect()
        {
            try
            {
                socket.BeginConnect(ipEndPoint, new AsyncCallback(ConnectCallback), socket);
                OnLog?.Invoke(this, new LogMessage("서버로 연결 요청"));
            }
            catch (Exception ex)
            {
                OnLog?.Invoke(this, new LogMessage("Client Connect error : " + ex.Message));
            }
        }

        private void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                socket.EndConnect(ar);
                OnLog?.Invoke(this, new LogMessage("연결 성공"));
                IsConnected = true;
                OnConnect?.Invoke(this, EventArgs.Empty);
                
                socket.BeginReceive(ReceiveBuffers, SocketFlags.None, new AsyncCallback(ReceiveCallback), socket);
                OnLog?.Invoke(this, new LogMessage("수신 대기중..."));

                SendSocketInfo();
            }
            catch (Exception ex)
            {
                OnLog?.Invoke(this, new LogMessage("Client Connect&Receive error : " + ex.Message));
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            //OnLog?.Invoke(this, new LogMessage("데이터 수신"));
            if (!IsConnected) return;
            try
            {
                int dataLength = socket.EndReceive(ar);
                if (dataLength > 0)
                {
                    //OnLog?.Invoke(this, new LogMessage("데이터 있음"));
                    ReceiveDataAnalysis(dataLength);
                    socket.BeginReceive(ReceiveBuffers, SocketFlags.None, new AsyncCallback(ReceiveCallback), socket);
                }
                else
                {
                    // 데이터가 0이면 종료신호
                    Disconnect();
                }

            }
            catch (Exception ex)
            {
                OnLog?.Invoke(this, new LogMessage("Client Receive error : " + ex.Message));
            }
        }

        private void ReceiveDataAnalysis(int dataLength)
        {
            for (int i = 0; i < dataLength; i++)
            {
                ReceiveBytes.Add(ReceiveBuffer[i]);
                if (ReceiveBuffer[i] == StaticClass.EOTR) // 끝이면 OnReceive 실행
                {
                    ReceiveBytes.RemoveAt(0);
                    ReceiveBytes.RemoveAt(ReceiveBytes.Count - 1);

                    OnLog?.Invoke(this, new LogMessage("데이터 수신"));
                    // 수신 이벤트 발생 함수
                    OnReceive?.Invoke(socket, new DataArg<string>(Encoding.UTF8.GetString(ReceiveBytes.ToArray())));

                    ReceiveBytes.Clear();
                }
            }
        }

        private void SendSocketInfo()
        {
            try
            {
                ClientSocketInfo info = new ClientSocketInfo
                {
                    groupID = GroupID
                };
                string msg = JsonConvert.SerializeObject(info, Formatting.Indented);

                byte[] byteData = SetHeader(msg, true);

                socket.BeginSend(byteData, 0, byteData.Length, SocketFlags.None, new AsyncCallback(SendCallback), socket);
                OnLog?.Invoke(this, new LogMessage("소켓 정보 셋팅중..."));
            }
            catch (Exception ex)
            {
                OnLog?.Invoke(this, new LogMessage("Client Info Send error : " + ex.Message));
            }
        }

        private byte[] SetHeader(string msg, bool setInfo = false)
        {
            byte[] temp = Encoding.UTF8.GetBytes(msg);
            byte[] byteData = new byte[1 + temp.Length + 1];
            byteData[0] = (setInfo) ? StaticClass.SOH : StaticClass.SOT; // START OF HEADING || START OF TEXT
            byteData[temp.Length + 1] = StaticClass.EOTR; //END OF TRANSMISSION
            Array.Copy(temp, 0, byteData, 1, temp.Length);

            return byteData;
        }

        public void SendMessage(string msg)
        {
            try
            {
                byte[] byteData = SetHeader(msg);
                socket.BeginSend(byteData, 0, byteData.Length, SocketFlags.None, new AsyncCallback(SendCallback), socket);
                OnLog?.Invoke(this, new LogMessage("데이터 송신 시작"));
            }
            catch (Exception ex)
            {
                OnLog?.Invoke(this, new LogMessage("Client Send error : " + ex.Message));
            }
        }

        private void SendCallback(IAsyncResult ar)
        {
            if (socket.EndSend(ar) >= 0)
            {
                OnLog?.Invoke(this, new LogMessage("데이터 송신 성공"));
                OnSend?.Invoke(this, EventArgs.Empty);
            }
        }
        public void Disconnect()
        {
            if (!IsConnected) return;
            try
            {
                socket.BeginDisconnect(false, new AsyncCallback(DisconnectCallback), socket);
                OnLog?.Invoke(this, new LogMessage("연결 종료 요청"));
            }
            catch (Exception ex)
            {
                OnLog?.Invoke(this, new LogMessage("Client BeginDisconnect error : " + ex.Message));
            }
        }
        private void DisconnectCallback(IAsyncResult ar)
        {
            try
            {
                socket.EndDisconnect(ar);
            }
            catch (Exception ex)
            {
                OnLog?.Invoke(this, new LogMessage("Client EndDisconnect error : " + ex.Message));
            }
            OnDisconnect?.Invoke(this, EventArgs.Empty);
            socket.Close();
            IsConnected = false;

            GroupID = null;

            OnConnect = null;
            OnReceive = null;
            OnSend = null;
            OnDisconnect = null;

            OnLog?.Invoke(this, new LogMessage("연결 종료 완료"));
        }
    }
}
