﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SocketMCClass.ServerSocket
{
    public class CSocket
    {
        Socket clientSocket;
        IPEndPoint serverIP;
        IPEndPoint clientIP;
        string groupID;

        byte[] receiveBuffer = new byte[StaticClass.MAX_RECEIVE_LENGTH];
        List<ArraySegment<byte>> receiveBuffers = new List<ArraySegment<byte>>();
        List<byte> receiveBytes = new List<byte>();

        public CSocket(Socket clientSocket)
        {
            ClientSocket = clientSocket;
            ServerIP = (IPEndPoint)ClientSocket.LocalEndPoint;
            ClientIP = (IPEndPoint)ClientSocket.RemoteEndPoint;

            receiveBuffers.Add(new ArraySegment<byte>(ReceiveBuffer, 0, StaticClass.MAX_RECEIVE_LENGTH));

        }
        public CSocket(Socket clientSocket, string groupID) : this(clientSocket)
        {
            GroupID = groupID;
        }
        
        public string GroupID { get => groupID; set => groupID = value; }
        public Socket ClientSocket { get => clientSocket; set => clientSocket = value; }
        public IPEndPoint ServerIP { get => serverIP; private set => serverIP = value; }
        public IPEndPoint ClientIP { get => clientIP; private set => clientIP = value; }
        public byte[] ReceiveBuffer { get => receiveBuffer; set => receiveBuffer = value; }
        public List<ArraySegment<byte>> ReceiveBuffers { get => receiveBuffers; set => receiveBuffers = value; }

        public List<byte> ReceiveBytes { get => receiveBytes; set => receiveBytes = value; }
    }
    public class SSocketMC
    {
        Socket serverSocket;
        List<CSocket> clientSockets = new List<CSocket>();
        IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 55551);
        Boolean isRunning;
        
        public event EventHandler OnStart;
        public event EventHandler OnConnect;
        public event EventHandler OnReceive;
        public event EventHandler OnSend;
        public event EventHandler OnDisconnect;
        public event EventHandler OnStop;
        public event EventHandler OnLog;

        public SSocketMC()
        {
            serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }
        public SSocketMC(IPEndPoint localEndPoint) : this()
        {
            this.localEndPoint = localEndPoint;
        }
        public SSocketMC(string ipAddress, string portNumber) : this()
        {
            localEndPoint = StaticClass.GetIPEndPoint(ipAddress, portNumber);
        }

        internal List<CSocket> ClientSockets { get => clientSockets; }

        public void Start()
        {
            if (isRunning) return;
            try
            {
                serverSocket.Bind(localEndPoint);
                serverSocket.Listen(100);
                serverSocket.BeginAccept(AcceptCallback, serverSocket);
                isRunning = true;
                OnLog?.Invoke(this, new LogMessage(localEndPoint + " 서버 시작"));
                OnLog?.Invoke(this, new LogMessage("클라이언트 연결 대기중..."));
                OnStart?.Invoke(this, EventArgs.Empty);
            }
            catch (Exception ex)
            {
                OnLog?.Invoke(this, new LogMessage("Start error : " + ex.Message));
            }
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            //서버소켓 닫을때 AcceptCallback이 호출된다. 왜지? -> BeginAccept가 영원히 대기하는것을 방지하기 위함인듯
            //OnLog?.Invoke(this, new LogMessage("요청 발생 테스트 부분")); 
            if (!isRunning) return;
            try
            {
                Socket clientSocket = serverSocket.EndAccept(ar);
                CSocket cSocket = new CSocket(clientSocket);
                clientSockets.Add(cSocket);
                OnLog?.Invoke(this, new LogMessage(cSocket.ClientIP + "에서 연결 요청"));
                OnLog?.Invoke(this, new LogMessage(cSocket.ClientIP + "의 연결 요청 수락"));
                OnConnect?.Invoke(cSocket, EventArgs.Empty);

                // 연결된 소켓 데이터 받기
                ReceiveMessage(cSocket);

                serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), serverSocket);
            }
            catch (ObjectDisposedException ODEx)
            {
                OnLog?.Invoke(this, new LogMessage("서버 종료 후처리 작업"));
            }
            catch (Exception ex)
            {
                OnLog?.Invoke(this, new LogMessage("Accept error : " + ex.Message));
            }
        }

        private void ReceiveMessage(CSocket cSocket)
        {
            OnLog?.Invoke(this, new LogMessage(cSocket.ClientIP + "의 데이터 수신 대기중..."));
            cSocket.ClientSocket.BeginReceive(cSocket.ReceiveBuffers, SocketFlags.None, new AsyncCallback(ReceiveCallback), cSocket);
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            // 서버소켓 닫을(루프돌며 닫기요청)때 ReceiveCallback이 실행된다. 왜지? -> BeginReceive가 영원히 대기하는것을 방지하기 위함인듯
            CSocket cSocket = (CSocket)ar.AsyncState;
            try
            {
                int dataLength = cSocket.ClientSocket.EndReceive(ar); // 데이터 수신
                //OnLog?.Invoke(this, new LogMessage(cSocket.ClientIP + "의 데이터 수신"));

                if (dataLength > 0) // 데이터 있음
                {
                    ReceiveDataAnalysis(cSocket, dataLength);
                    cSocket.ClientSocket.BeginReceive(cSocket.ReceiveBuffers, SocketFlags.None, new AsyncCallback(ReceiveCallback), cSocket);
                }
                else
                {
                    // 데이터가 0이면 연결 해제 요청
                    Disconnect(cSocket);
                }

            }
            catch (SocketException SEx) // 강제로 끊겼을 때
            {
                clientSockets.Remove(cSocket);
                OnLog?.Invoke(this, new LogMessage("클라이언트 측에서 연결이 강제로 끊어졌습니다.\n해당 클라이언트를 리스트에서 제거하였습니다."));
            }
            catch (ObjectDisposedException ODEx) // 소켓이 이미 종료되었을 때 (서버 종료시 발생)
            {
                clientSockets.Remove(cSocket);
                OnLog?.Invoke(this, new LogMessage("이미 종료된 클라이언트입니다.\n해당 클라이언트를 리스트에서 제거하였습니다."));
            }
            catch (Exception ex)
            {
                OnLog?.Invoke(this, new LogMessage("Recevice error : " + ex.Message));
            }
        }
        private void ReceiveDataAnalysis(CSocket cSocket, int dataLength)
        {
            for (int i = 0; i < dataLength; i++)
            {
                cSocket.ReceiveBytes.Add(cSocket.ReceiveBuffer[i]);
                if (cSocket.ReceiveBuffer[i] == StaticClass.EOTR) // 끝이면 OnReceive 실행
                {
                    if (cSocket.ReceiveBytes[0] == StaticClass.SOH) // 클라이언트 정보 패킷이면
                    {
                        cSocket.ReceiveBytes.RemoveAt(0);
                        cSocket.ReceiveBytes.RemoveAt(cSocket.ReceiveBytes.Count - 1);
                        SetClientSocketInfo(cSocket);
                    }
                    else if (cSocket.ReceiveBytes[0] == StaticClass.SOT) // 아니면
                    {
                        cSocket.ReceiveBytes.RemoveAt(0);
                        cSocket.ReceiveBytes.RemoveAt(cSocket.ReceiveBytes.Count - 1);
                        OnLog?.Invoke(this, new LogMessage("데이터 수신"));
                        // 수신 이벤트 발생 함수
                        OnReceive?.Invoke(cSocket, new DataArg<string>(Encoding.UTF8.GetString(cSocket.ReceiveBytes.ToArray())));
                    }
                    cSocket.ReceiveBytes.Clear();
                }
            }
        }
        

        private void SetClientSocketInfo(CSocket cSocket)
        {
            try
            {
                ClientSocketInfo info = JsonConvert.DeserializeObject<ClientSocketInfo>(Encoding.UTF8.GetString(cSocket.ReceiveBytes.ToArray()));
                //cSocket.UserID = info.userID;
                cSocket.GroupID = info.groupID;
                OnLog?.Invoke(this, new LogMessage("클라이언트 정보 설정 완료"));
            }
            catch (Exception ex)
            {
                OnLog?.Invoke(this, new LogMessage("클라이언트 정보 설정에 실패하였습니다."));
            }
        }

        private byte[] SetHeader(string msg)
        {
            byte[] temp = Encoding.UTF8.GetBytes(msg);
            byte[] byteData = new byte[1 + temp.Length + 1];
            byteData[0] = StaticClass.SOT; // START OF TEXT
            byteData[temp.Length + 1] = StaticClass.EOTR; //END OF TRANSMISSION
            Array.Copy(temp, 0, byteData, 1, temp.Length);

            return byteData;
        }
        public void SendMessageGroup(string clientGroupID, string msg)
        {
            clientSockets.ForEach(cSocket => {
                if (cSocket.GroupID == clientGroupID)
                {
                    SendMessage(cSocket, msg);
                }
            });
        }
        public void BroadcastMessage(string msg)
        {
            clientSockets.ForEach(cSocket => {
                    SendMessage(cSocket, msg);
            });
        }
        public void SendMessage(CSocket cSocket, string msg)
        {
            try
            {
                byte[] byteData = SetHeader(msg);
                cSocket.ClientSocket.BeginSend(byteData, 0, byteData.Length, SocketFlags.None, new AsyncCallback(SendCallback), cSocket);
                OnLog?.Invoke(this, new LogMessage(cSocket.ClientIP + "로 데이터 송신 시작"));
            }
            catch (Exception ex)
            {
                OnLog?.Invoke(this, new LogMessage("Send error : " + ex.Message));
            }
        }
        private void SendCallback(IAsyncResult ar)
        {
            CSocket cSocket = (CSocket) ar.AsyncState;
            if (cSocket.ClientSocket.EndSend(ar) >= 0)
            {
                OnLog?.Invoke(this, new LogMessage(cSocket.ClientIP + "로 데이터 송신 성공"));
                OnSend?.Invoke(cSocket, EventArgs.Empty);
            }
            
        }

        public void Stop()
        {
            if (!isRunning) return;
            OnLog?.Invoke(this, new LogMessage(localEndPoint + " 서버 종료 시작"));
            OnLog?.Invoke(this, new LogMessage("클라이언트 소켓 " + clientSockets.Count + "개의 연결 해제"));

            for (int i = clientSockets.Count-1; i >= 0; i--)
            {
                Disconnect(clientSockets[i]);
            }
            
            OnStop?.Invoke(this, EventArgs.Empty);
            serverSocket.Dispose();

            OnStart = null;
            OnConnect = null;
            OnReceive = null;
            OnSend = null;
            OnDisconnect = null;
            OnStop = null;

            isRunning = false;
            OnLog?.Invoke(this, new LogMessage(localEndPoint + " 서버 종료 완료"));
        }

        private void Disconnect(CSocket cSocket)
        {
            //if (!cSocket.IsConnected) return;
            try
            {
                OnLog?.Invoke(this, new LogMessage(cSocket.ClientIP + "의 연결 해제 요청"));
                cSocket.ClientSocket.BeginDisconnect(false, new AsyncCallback(DisconnectCallback), cSocket);
            }
            catch (Exception ex)
            {
                OnLog?.Invoke(this, new LogMessage("Client socket BeginDisconnect error : " + ex.Message));
            }
        }

        private void DisconnectCallback(IAsyncResult ar)
        {
            CSocket cSocket = (CSocket)ar.AsyncState;
            try
            {
                cSocket.ClientSocket.EndDisconnect(ar);
            }
            catch (Exception ex)
            {
                OnLog?.Invoke(this, new LogMessage("Client socket EndDisconnect error : " + ex.Message));
            }
            cSocket.ClientSocket.Close();
            
            OnLog?.Invoke(this, new LogMessage(cSocket.ClientIP + "의 연결 해제 성공"));
            OnDisconnect?.Invoke(cSocket, EventArgs.Empty);
            clientSockets.Remove(cSocket);
        }

        /* 편의 */
    }
}
